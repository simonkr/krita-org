# Krita.org website

Marketing website for the open source art application Krita. Built with the Hugo static site generator. Currently deployed at https://dev.krita.org

## Building and testing the site locally

This site is based on [hugo-kde theme](https://invent.kde.org/websites/hugo-kde/), which is the common theme for various KDE websites that are based on Hugo. Follow [the guide on its wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/Getting-started) to know what are needed to use the theme and how to build a website with Hugo.

If you see a white page, you might have to run this before the "hugo server" command

    hugo mod clean

This usually means there was an update to the hugo theme, and your cached version if messing it up.

## Development Notes

See the [DEVELOPMENT](/DEVELOPMENT.md) guide for more information about technical changes.

## Day-to-day tasks

See the [USAGE](/USAGE.md) guide for more information on creating posts, pages, and doing releases through the Web IDE.
