# baseURL: https://krita.org
baseURL: https://krita.org/  # use this baseURL while pushing to websites repo for now

summaryLength: 25
hasCJKLanguage: true
rssLimit: 20
defaultContentLanguageInSubdir: true # makes 'en' appear in URL since it is default language

markup:
  goldmark:
    extensions:
      typographer: false
    renderer:
      unsafe: true
  highlight:
    noClasses: false
module:
  imports:
  - path: invent.kde.org/websites/hugo-kde

params:
  madeByKde: false
  rssSection: posts
  searchable: true
  defaultLangInSubdir: true
  colorModeType: data

sitemap:
  changefreq: monthly
  filename: sitemap.xml
  priority: 0.5

# can access JSON output by going to site roots like: http://localhost:1313/ja/index.jsn
outputs:
  home:
  - HTML
  - RSS
  - JSON # this will be data source used for site search

# i18n section is KDE translation pipeline
# see https://invent.kde.org/websites/hugo-i18n for details
# globs format is not recursive, so need to list out all folders
i18n:
  excludedKeys: imageUrl buyUrl categories
  srcDir: content/en
  genDir: content
  genToOtherDir: true
  shortcodes:
    params:
      sponsor-item: [alt]
  others: [strings, description, menu, title]
  content:
    pages:
      files:
      - content/en/categories/artist-interview/_index.md
      - content/en/posts/_index.md
      # Everything in these directories will be translated through the KDE i18n team
      # Note: All the directories use a / as the folder separator. The Python hugoi18n
      # script needs this to extract the files correctly
      globs:
      - content/en/*.md
      - content/en/about/*.md
      - content/en/events/*.md
      - content/en/pages/*.md
    news:
      globs:
      - content/en/posts/2023/*.md
      - content/en/posts/2024/*.md
      - content/en/posts/2025/*.md
      - content/en/posts/2026/*.md
      - content/en/posts/2027/*.md
      - content/en/posts/2028/*.md
      
    release_note:
      globs:
      - content/en/release-notes/*.md
      excludedFiles: # only include last release notes and not backlog
      - content/en/release-notes/krita-2-9-the-kickstarter-release.md
      - content/en/release-notes/krita-3-0-release-notes.md
      - content/en/release-notes/krita-3-1-2.md
      - content/en/release-notes/krita-4-0-release-notes.md
      - content/en/release-notes/krita-4-1-release-notes.md
      - content/en/release-notes/krita-4-2-release-notes.md
      - content/en/release-notes/krita-4-3-release-notes.md
      - content/en/release-notes/krita-4-4-0-release-notes.md
      - content/en/release-notes/release-notes-for-3-1-2.md
      - content/en/release-notes/release-notes-for-3-1-3.md
      - content/en/release-notes/release-notes-for-krita-3-1.md
      - content/en/release-notes/release-notes-for-krita-3-2.md
languages:
  en:
    contentDir: content/en
    languageName: English
    menu:
      main:
      - name: Learn
        params:
          isExternal: true
        url: https://docs.krita.org
        weight: 3
    params:
      description: Krita is a professional FREE and open source painting program.
        It is made by artists that want to see affordable art tools for everyone.
    title: Krita
    weight: 1
  ja:
    contentDir: content/ja
    languageCode: ja
    languageName: 日本語
    menu:
      main:
      - name: Learn
        params:
          isExternal: true
        url: https://docs.krita.org
        weight: 3
    params:
      description: Krita is a professional FREE and open source painting program.
        It is made by artists that want to see affordable art tools for everyone.
    title: Krita
    weight: 2
