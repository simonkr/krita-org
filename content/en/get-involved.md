---
title: 'Get Involved'
layout: simple
date: '2018-05-12'
menu:
  main:
    weight: 4
---

We always welcome new contributors! Krita is a huge and ambitious project. There is more work than our small core team can handle. The Krita community is very open, welcoming and supportive. No resumé or CV needed! If you've got the enthusiasm, you're all welcome. [Get in contact with us](contact/) if you have any issues getting started.

### Testers

Programmers are going to create bugs and problems as new features get added. Help us make Krita more stable by testing out the newest features.

**How can I help?**
1. Download the latest version of Krita from the nightly builds. These versions are close to what developers use.
2. Find bugs and report them to our bug tracker. We have an extensive guide on how to report and work with bugs.

{{< button url="https://docs.krita.org/en/untranslatable_pages/reporting_bugs.html" target="_blank" >}}

Learn more about reporting bugs

{{< /button >}}

### Translators

You can help Krita reach a more international audience by translating the program itself or the user documentation.

**How can I help?**

Get in touch with the KDE i18n team and say you want to help translate Krita. KDE helps us manage our website, hosting, downloads, and translations.

{{< button url="https://docs.krita.org/en/contributors_manual/krita_manual_readme.html?#translating" target="_blank" >}}

Learn more about translation process

{{< /button >}}

### Developers

You'll be able to work on one of the coolest and fastest-growing open source painting programs out there. Krita is built with Qt and C++.

**How can I help?**

1. Take a look at our documentation on building the source code.
2. If you any issues and want some guidance on bugs to pick off, contact the developers to get you in the right direction.

{{< button url="https://docs.krita.org/en/untranslatable_pages/intro_hacking_krita.html" target="_blank" >}}

See developer documentation

{{< /button >}}

### Documentation

The learning area always needs some extra work. Help tell the world about features that are missing in the documentation, or new features that are coming to the next version.

**How can I help?**

1. Take a look at the contribution guide. It has information about how the documentation site works as well as making changes.
2. Submit your changes for review

{{< button url="https://docs.krita.org/en/contributors_manual/krita_manual_readme.html" target="_blank" >}}

See Manual Contributors Guide

{{< /button >}}

### Artwork

We'd love to see what you are creating with Krita!

**How can I help?**

1. Post your artwork in the forum and let us know if we can use it for promotional purposes.
2. Add @krita_painting to your tweets or add a reference that you made your artwork with Krita

{{< button url="https://krita-artists.org/c/artwork/l/top" target="_blank" >}}

Visit the forum

{{< /button >}}

### Tutorials, Brushes, Patterns, Python Plugins

Do you have resources to share and contribute? They are always welcome!

**How can I help?**

1. Share your tutorials, brush packs, or other resources you make in the forum. The developers get notified when things are posted here.
2. We will add some resources and tutorial links to our official documentation site if we think it is a good fit.

{{< button url="contact/" >}}

Contact developers with ideas

{{< /button >}}

### Bug Triaging

If someone reports a bug, it might not actually be a bug. The person might not just know how to use the feature, or the feature might not even exist!

**How can I help?**

Look through our bug tracker and help us determine if a report is a bug or new feature request.

{{< button url="https://docs.krita.org/en/untranslatable_pages/triaging_bugs.html" target="_blank" >}}

Check out the triaging page

{{< /button >}}

### Donations

While you might not have enough time to help out in other areas, donations are always appreciated to help other people do this work.

**How can I help?**

Donations are used for a multitude of things to help improve Krita and grow the community.

{{< button url="donations/" >}}

Read about what donations can do

{{< /button >}}

## Feature Requests and Suggestions

Do you have ideas to make Krita into an even better program? Great! But before you "report to wishlist", please follow the steps below:

1. Check out [this manual page](https://docs.krita.org/en/untranslatable_pages/new_features.html) on making feature requests!
2. You should then discuss your ideas on the [Forum](https://forum.kde.org/viewforum.php?f=136) or [Krita Artists](https://krita-artists.org/) to get feedback from other Krita users.
3. When your ideas have ripened, it's time to make a feature request (a wish) in [Krita's issue tracker](https://bugs.kde.org).

Please keep in mind that it may take years before a feature is implemented: there are a lot of things that could be done, but only a relatively tiny team of developers. You can help speed this up a bit by working with the developers on additional details such as the graphical interface, as well as providing links to relevant resources. Here is an example of a [good feature proposal write-up](http://www.davidrevoy.com/article159/design-ideas-for-a-new-krita-perspective-tool).

