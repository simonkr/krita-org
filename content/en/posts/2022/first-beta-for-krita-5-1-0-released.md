---
title: "First Beta for Krita 5.1.0 Released"
date: "2022-06-23"
categories: 
  - "development-builds"
  - "news"
---

We're releasing the first beta for Krita 5.1.0 today. Krita 5.1.0 is packed with new features! For the full list, check out the work-in-progress full [release notes](en/release-notes/krita-5-1-release-notes/)!

{{< support-krita-callout >}}

## Highlights

- Even more operations can handle multiple selected layers
- We've improved support for the WebP, Photoshop layered TIFF and Photoshop files, and there's new support for the JPEG-XL file format. (See the warning below, though!)
- We're now using XSIMD instead of VC, improving painting performance, especially on Android where vectorization now is used for the first time.
- The fill tools have been extended with continuous fill and a new Enclose and fill tool.
- For Windows, we're using a newer version of Angle to improve compatibility with video drivers and improve performance.
- You can now configure touch controls in canvas input settings, like "Tap to Undo".

And of course, there are hundreds of bug fixes, performance improvements, user interface refinements, improvements to the animation system (though the revamped audio system sadly didn't make it, that's for 5.2).

## Known Issues

- Touch gestures can be customized now, however if you were previously using touch gestures, you will now have to add them to the canvas input settings before they work again.
- Though this release add support for the new JPEG-XL format, which supports animation, exporting and importing animations in this format gives incorrect results. A fix is pending.

![Screenshot of 5.1.0-beta1](images/posts/2022/5.1.0-beta1-1024x562.webp)

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.1.0-beta1-setup.exe](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x64-5.1.0-beta1-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.1.0-beta1.zip](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x64-5.1.0-beta1.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x64-5.1.0-beta1-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.1.0-beta1-x86\_64.appimage](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1-x86_64.appimage)

The separate gmic-qt appimage is no longer needed. Note: If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.1.0-beta1.dmg](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1.dmg)

### Android

The Android releases are made from the release tarball, so there are translations. We consider Krita on ChromeOS and Android still **_beta_**.

- [64 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x86_64-5.1.0-beta1-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x86-5.1.0-beta1-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-arm64-v8a-5.1.0-beta1-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-armeabi-v7a-5.1.0-beta1-release-signed.apk)

### Source code

- [krita-5.1.0-beta1.tar.gz](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1.tar.gz)
- [krita-5.1.0-beta1.tar.xz](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1.tar.xz)
