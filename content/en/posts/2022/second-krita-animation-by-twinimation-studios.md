---
title: "Second Krita Animation by Twinimation Studios"
date: "2022-07-20"
---

_Today we’ve got a second guest post by Andria Jackson who has created a new course on using Krita to create 2D animation!_

Hello, Twinimation Studios is back with a brand new course and a brand new website! If you love old timey, Fleischer’s style animations like Betty Boop and Koko the Clown, you can learn how to imitate these styles to create your own rubberhose work of art! Introducing Twinimation’s newest **_22 lesson, 6 hour course!_** You will see the process of replicating rubberhose film through music, animation method, character design, and post production templates.

Once again, we’re using Krita for our film lesson, but we’re going even more in depth in this course. We’ll be showing off more of Krita’s features, such as how to color using the paint bucket tool and the benefits of changing frame colors. Additionally, in this course, you’ll see Krita not only used for animation, but also illustration as we design and color background environments.

Twinimation Studios was founded by instructors Andria and Arneisha Jackson. Andria and Arneisha are MFA graduates who've studied animation for 7 years and are now ready to share their professional knowledge with the world. You will be able to learn different styles of animation, character design, environment design, illustration, 3D modeling, film creation and so much more from the comfort of your own home and at your own pace! The student is free to choose how quickly they want to learn, which order they'd like to handle their lessons, and so on. We plan to provide a large array of animation courses here on Twinimation, so eventually you will be able to pick and choose which elements you'd like to learn, and all for very reasonable prices. We believe in providing you with all of the information we feel you need to know in order to create beautiful portfolio pieces to start your journey into the industry.

Here is a teaser for this newest course:

{{< youtube bftqScOMKn4 >}}


### Course links
And here is a link to the course on our brand new website! [https://twinimationstudios.podia.com/animation-through-the-ages-rubberhose](https://twinimationstudios.podia.com/animation-through-the-ages-rubberhose)
