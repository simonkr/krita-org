---
title: "What the Krita Developers Are Up To, Part II"
date: "2022-08-07"
categories: 
  - "news"
---

Krita 5.1 is nearing its release, and working on that has claimed a lot of our attention. Not that there haven't been other things happening!

And not just bug reports, though with on average a hundred new bug reports a month, that's taking a lot of work as well. Even if the bug report is more a cry for user support than a real bug report, it will take time to evaluate and close. Say, on average, half an hour of engagement with the report and reporter before the real work starts, just triaging these bugs takes 50 hours a month!

At the same time, every artist who starts using Krita will have ideas that must be implemented, so there is a never ending stream of feature requests, often of the form "Photoshop has X, it's incredible that Krita still hasn't X, too!". That is not only discouraging, it is also a big timesink. And since there's so much of it, it's not possible for the Krita developers to meaningfully engage with all the ideas artists bring to the table.

Working in response to bugs and feature requests is also reactive: it prevents the Krita team from developing a vision, and then using that vision to develop and improve Krita.

And that's just frustrating. Some of us have been working on Krita for more than a decade. Some for nearly two decades, in fact, and we know what we want Krita to be.

## Krita's competition
So earlier this year, we sat down together, every developer who could make the virtual meeting to discuss what fun stuff we wanted to work on after all the pandemic depression, resource system rewrite and bug fixing. First, Wolthera and Halla sat down together to investigate our closest competitor. Which application is that?

If you guessed Photoshop, you guessed wrong. Honestly, we think Krita's painting tools are better than Photoshop's. Some people might disagree, will disagree -- just like there are always people who want better performance, always.

It's also not Affinity Photo. Or Corel Painter.

We think that Clip Studio is much more of a real competitor: that is also an application created for working artists who need to be productive to earn money. Corel Painter's niche nowadays is tracing wedding photographs in a painterly style, Affinity doesn't have a strong brush enging at all, and Photoshop is copying our features, instead of the other way around.

[Here is the first document we created](https://docs.google.com/document/d/18ZL_DiMc3VdtcN_dCz_BTMn1fR3M72GuRw2E4ThRdiw) where we talk about what we think is lacking in Krita compared to Clip Studio: let's call it a "competitor analysis".

Then we met up, virtually, of course, and discussed what we wanted to do. That also resulted in [a document you can browse at your leisure](https://docs.google.com/document/d/1GPcclrQbxZN7iDeSP6Zo3CckEN8rrotwYozjmjktmX8). This actually has names with the topics!

But let's pick out some things. Let's start with text. People complain a lot about Krita's text tool, though in reality, apart from doing the editing in a popup instead of on the canvas, it was already more powerful than Clip Studio's. Now there are two parts to text: the actual text object that renders text on the canvas, and the tool, that allows you to enter text and change text properties like the font.


## Updates to Text technology
[Wolthera has completely rewritten the rendering engine for the text object](https://invent.kde.org/graphics/krita/-/merge_requests/1403#note_462733). It no longer uses Qt's text layout classes, since those were too limited. (Apparently, they were written for rendering Qt's help files, though that might be a retcon, since I remember that they were presented as a full-blown layout engine.) That part is done! It will be merged right after the Krita 5.1 release, because it needs new dependencies, like [libraqm](https://github.com/HOST-Oman/libraqm).


[![The new capabilities of the text shape](images/posts/2022//text-775x1024.webp)


Note: changing the text tool to work on text on-canvas is something we want to work on, but out of scope for _this_ project!

## Other developer progress of projects
[Emmet and Eoin](https://invent.kde.org/graphics/krita/-/merge_requests/1323) have completely rewritten the audio part of Krita's animation support. We used to use QMultiMedia, but that library has a very strange API and never worked all that well; apparently its intention was simply for playing sounds in mobile games. Or something. This has taken months of work, of course! But right now, [the first test build, Linux only, is ready](https://krita-artists.org/t/animation-audio-development-test-linux-appimage/45106)! [Here is their vision for Animation in Krita](https://docs.google.com/document/d/1iUclKym_8Ta7Z3_ADhxdGdZFlQcFOqMQETphVCGhP1M).

Back in 2007, it seemed a good idea to build the brush preset editor around the user interface objects that change the preset settings. Yes, we were young and foolish. [Dmitry is working on refactoring that extremely complicated and error prone architecture using a library called Lager. Dmitry is, of course, also always working on improving Krita's performance and memory handling, and chunks of that are already in Krita 5.1!](https://invent.kde.org/graphics/krita/-/merge_requests/1334)

Since there are many other places in Krita where we did the same thing -- keep the state of something in the user interface, like in the tools, this opens up some exciting possibilities, like [tool presets](https://invent.kde.org/graphics/krita/-/merge_requests/559), or even [multiple tool instances](https://phabricator.kde.org/T15062). That's something Halla has wanted to work on for some time. But she's also doing the admin work of maintaining a big free software project, so whether she'll get down to it... That's an open question.

[Sharaf, our Android maintainer, is looking into bringing back the mobile-friendly UX we used to have for Krita 4](https://invent.kde.org/graphics/krita/-/merge_requests/1522). Or rather, rewrite it from scratch with modern QML and Kirigami. He has started with the welcome page. Right now, Krita's welcome page is intentionally a bit drab and understated, but we want to do much more with it: show off featured artwork from [Krita Artists](https://krita-artists.org) (and just remember how new _that_ website is, and how central a part of the Krita community it has become), links to tutorials and videos, news from this website, recent projects -- or maybe a proper project organizer for all projects.

[Agata is working on painting assistants](https://phabricator.kde.org/T13380). Krita's painting assistants have always been a really powerful tool marred by by a relatively poor usability and a really confusing codebase. Already three different merge requests have been merged into what is going to become Krita 5.2, and more is to come! She was also really helped out by [the community at Krita Artists](https://krita-artists.org/t/planned-changes-to-assistants-coming-in-2022-or-later/43064).

[Also already merged is AmySpark's brand new jpeg-xl file format plugin.](https://invent.kde.org/graphics/krita/-/merge_requests/1363) This is a format that supports both animation as well as still images with very good lossless compression.

## Stability and volunteer contributors
And, of course, all this work doesn't mean we're neglecting stability, performance or even just keeping up with new compilers, updated dependencies or smaller features. We solved over 500 bugs in the past half year... And this is just what the sponsored developers are doing and planning to do: there are also many volunteer developers making awesome contributions to Krita.

[Freya](https://invent.kde.org/graphics/krita/-/merge_requests?scope=all&state=all&author_username=freyalupen) has improved the display of layers in the layer box so it optionally shows blending mode and opacity, added single direction wraparound mode and zoom fit with margins.

[Deif Lou](https://invent.kde.org/graphics/krita/-/merge_requests?scope=all&state=all&author_username=deiflou)'s contributions are maybe too many too enumerate... Ranging from improving the fill tool so it can use label colors, to improving the fill tool's performance, to new filters -- to entire new tools, like the Enclose and Fill tool.

[Alvin](https://invent.kde.org/graphics/krita/-/merge_requests?scope=all&state=all&author_username=alvinwong) has worked on updating Krita to use the latest version of the Angle wrapper that sits between our OpenGL code and the Windows Direct3D display driver layer -- as well as a host of other things.

And there's more... But this is enough for today. Please support the Krita Foundation so we can continue our work and make Krita a great application for every digital artist! We have over 80,000 downloads a month -- and only about 4000 euros in donations, and that's the sum of the Krita dev fund and one-off donations!

{{< support-krita-callout >}}
