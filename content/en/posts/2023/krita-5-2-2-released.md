---
title: "Krita 5.2.2 Released!"
date: "2023-12-07"
categories: 
  - "news"
  - "officialrelease"
---

We bring you another 5.2 series bug fix release with Krita 5.2.2! Check out [release notes](en/release-notes/krita-5-2-release-notes) for everything that was new in Krita 5.2.0.

Some of the highlights in this bug fix release is that we managed to track down a frequent crash on android, as well as fixing a bug where brush presets with masked brushes were not loaded correctly.

- Make sure that the image is properly updated on frame switch on merge ([Bug 476495](https://bugs.kde.org/show_bug.cgi?id=476495))
- Various fixes to the snap version, courtesy of Scarlett Moore
- Fix a race condition when merging non-animatied layer over an animated one ([Bug 476472](https://bugs.kde.org/show_bug.cgi?id=476472))
- Don’t enable combined opacity-flow option on masked brushes ([Bug 476506](https://bugs.kde.org/show_bug.cgi?id=476506))
- Disable checkability of Opacity and Flow options for Masked Brushes ([Bug 476505](https://bugs.kde.org/show_bug.cgi?id=476505) , [Bug 476076](https://bugs.kde.org/show_bug.cgi?id=476076))
- Fix redo of a flatten layer operation when it is animated ([Bug 476511](https://bugs.kde.org/show_bug.cgi?id=476511))
- Hide perspective ellipse if assistants are globally hidden, courtesey of Maciej Jesionowski.
- Fix Specific Color Selector changing to black ([Bug 476614](https://bugs.kde.org/show_bug.cgi?id=476614))
- Add brush tip rotate actions and and a angle slider.
- Extract the cache in KisMyPaintOpSettings to properly handle size updates ([Bug 476141](https://bugs.kde.org/show_bug.cgi?id=476141))
- Remove wrong assert ([Bug 476761](https://bugs.kde.org/show_bug.cgi?id=476761))
- Fix cancelling of the selection when doing two consequtive selections ([Bug 474525](https://bugs.kde.org/show_bug.cgi?id=474525))
- Fixes for Qt: MDI titles and unbalanced non-us shortcuts ([Bug 464175](https://bugs.kde.org/show_bug.cgi?id=464175))
- Draw assistants directly on the canvas ([Bug 361709](https://bugs.kde.org/show_bug.cgi?id=361709), [Bug 401940](https://bugs.kde.org/show_bug.cgi?id=401940))
- Various palette docker fixes courtesy of Mike Will
- Better integration with windows darkmode, setting the window frame to dark when this is enabled ([Bug 477266](https://bugs.kde.org/show_bug.cgi?id=477266))
- Use PreciseTimer type with KisPlaybackEngineQT ([Bug 476059](https://bugs.kde.org/show_bug.cgi?id=476059))
- Various fixes to shortcuts, among others, to get altgr to work as a shortcut ([Bug 459557](https://bugs.kde.org/show_bug.cgi?id=459557), [Bug 474912](https://bugs.kde.org/show_bug.cgi?id=474912), [Bug 469691](https://bugs.kde.org/show_bug.cgi?id=469691), [Bug 473923](https://bugs.kde.org/show_bug.cgi?id=473923))
- Fix jumps just adjusting brush size with ‘\[’ and ‘\]’ shortcuts ([Bug 456787](https://bugs.kde.org/show_bug.cgi?id=456787))
- Replace fcitx-qt5 with fcitx5-qt, which should help the look and feel of fcitx-based input methods.
- Increase xmlgui version to be bigger that 5.1.6, which should solve old toolbars being reset ([Bug 475698](https://bugs.kde.org/show_bug.cgi?id=475698))
- Fixes a very frequent crash caused by QAccesibility on Android ([Bug 472705](https://bugs.kde.org/show_bug.cgi?id=472705))

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.2.2-setup.exe](https://download.kde.org/stable/krita/5.2.2/krita-x64-5.2.2-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.2.2.zip](https://download.kde.org/stable/krita/5.2.2/krita-x64-5.2.2.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.2.2/krita-x64-5.2.2-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.2.2-x86\_64.appimage](https://download.kde.org/stable/krita/5.2.2/krita-5.2.2-x86_64.appimage)

The separate gmic-qt AppImage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.2.2.dmg](https://download.kde.org/stable/krita/5.2.2/krita-5.2.2.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface requires a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.2.2/krita-x86_64-5.2.2-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/stable/krita/5.2.2/krita-x86-5.2.2-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.2/krita-arm64-v8a-5.2.2-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.2/krita-armeabi-v7a-5.2.2-release-signed.apk)

### Source code

- [krita-5.2.2.tar.gz](https://download.kde.org/stable/krita/5.2.2/krita-5.2.2.tar.gz)
- [krita-5.2.2.tar.xz](https://download.kde.org/stable/krita/5.2.2/krita-5.2.2.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.2.2/](https://download.kde.org/stable/krita/5.2.2) and click on Details to get the hashes.

### Key

The Linux AppImage and the source .tar.gz and .tar.xz tarballs are signed. The signatures are [here](https://download.kde.org/stable/krita/5.2.2/) (filenames ending in .sig).

Please support Krita by making a donation or joining the development fund!


{{< support-krita-callout >}}