---
title: "New krita.org website launched"
date: "2024-02-15"
---

After a lot of work, we moved our website to a new Hugo based platform! A lot of time and energy went into making this new version. There were a number of reasons and issues that we moved the site.

1. __Translations__ - The previous website had a very messy translation process. There was so much manual work creating logins for translators, and uploading files to the web server every time new languages were added. With the new system, translation is built through our KDE translation pipeline. This is the same pipeline that is used for translating the application. This greatly simplifies the experience for both our web master and translation team.
2. __Site maintenance__ - While many updates could previously be done via the CMS, there were a lot of portions that needed special access to the web server files. This made doing updates like releases a coordination between a lot of people to make sure all the pieces in place. 
3. __Simplify site building and publishing__ - The new site is only using static files in the end. In the previous CMS, pages were dynamically generated from a database. There were many complicated layers of caching that were needed to make the site responsive and load fast. This created a number of instances where the entire krita.org went down because of some caching snafu.

All the files for the website and information on how our site process works can be [found on GitLab instance](https://invent.kde.org/websites/krita-org). 

## Special Thanks

Special thanks goes out to all the work that was involved in making this transition
- __Scott Petrovic__: Leading the charge with performing the bulk of the work and taking the project to completion.
- __Phu Nguyen__: A large amount of help with understanding the inner workings of the Hugo platform as well as many code contributions.
- __Wolthera van Hövell__: Site fixes and cleanup when porting the content to the new system. 
- __Alvin Wong__: Improvements to the translation and internationalization aspects of the site.
- __Ben Cooksley__: Helping configure and get the site on a CI/CD process that makes publishing changes seamless.
- All the people that provided feedback on krita-artists.org
