---
title: "發佈 Krita 5.1.1 修正版本"
date: "2022-09-14"
categories: 
  - "news_zh-hk"
  - "officialrelease_zh-hk"
---

今日我哋發佈咗 Krita 5.1.1 正式版本。依個係一個修正版本，主要修正咗兩項重大問題——一項係啟動軟件比正常耐咗好多嘅問題，另一項係喺複製向量圖層時出現軟件崩潰嘅問題。

另外，今次更新亦包含咗其他問題修正：

- 修正 macOS 系統原生觸控板手勢嘅支援。[BUG:456446](https://bugs.kde.org/show_bug.cgi?id=456446)
- 修正 Android 版本因為置換檔案冇被刪除而導致應用程式佔用空間增加嘅問題。
- 修正 Android 版本中一項或會導致啟動軟件時出現軟件崩潰嘅問題。[BUG:458907](https://bugs.kde.org/show_bug.cgi?id=458907)
- 修正幾項 MyPaint 筆刷引擎嘅問題。MyPaint 橡皮刷依家使用咗正確嘅混色模式。而喺使用 MyPaint 筆刷時，冇作用嘅混色模式選單會被停用。[BUG:453054](https://bugs.kde.org/show_bug.cgi?id=453054), [BUG:445206](https://bugs.kde.org/show_bug.cgi?id=445206)
- 動畫匯出：修正匯出影像序列嘅「起始編號」選項。[BUG:458997](https://bugs.kde.org/show_bug.cgi?id=458997)
- 修正 kritadefault.profile 畫布輸入設定檔被移除後出現嘅軟件崩潰問題。
- 喺讀取 ACO 調色板時，載入色票名稱。[BUG:458209](https://bugs.kde.org/show_bug.cgi?id=458209)
- 修正選取圖層時出現軟件崩潰嘅問題。[BUG:458546](https://bugs.kde.org/show_bug.cgi?id=458546)
- 改善下列滑動條嘅調整級數：淡化、寛高比、近似色選取閾值
- 喺移動可上色圖層節點時，淨係更新畫布一次。
- 喺所有平台上修正使用多個輔助尺時 OpenGL 畫布出現黑色方塊嘅問題。[BUG:401940](https://bugs.kde.org/show_bug.cgi?id=401940)
- 改善讀取 ZIP 檔案（包括 .kra、.ora 檔）嘅效能。
- 修正開啟單圖層 PSD 檔案嘅問題。[BUG:458556](https://bugs.kde.org/show_bug.cgi?id=458556)
- 將開始畫面上嘅更新通知連結設定為可點選。[BUG:458034](https://bugs.kde.org/show_bug.cgi?id=458034)
- JPEG-XL：修正線性 HDR 影像匯出同埋 16 位元浮點色匯入功能。[BUG:458054](https://bugs.kde.org/show_bug.cgi?id=458054)

 

## 下載

### Windows

如果你使用免安裝版：請注意，免安裝版仍然會同安裝版本共用設定檔同埋資源。如果想用免安裝版測試並回報 crash 嘅問題，請同時下載偵錯符號 (debug symbols)。

注意：我哋依家唔再提供為 32 位元 Windows 建置嘅版本。

- 64 位元安裝程式：[krita-x64-5.1.1-setup.exe](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1-setup.exe)
- 64 位元免安裝版：[krita-x64-5.1.1.zip](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1.zip)
- [偵錯符號 Debug symbols（請解壓到 Krita 程式資料夾入面）](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1-dbg.zip)

### Linux

- 64 位元 Linux: [krita-5.1.1-x86\_64.appimage](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1-x86_64.appimage)

Linux 版本而家唔使再另外下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果你用緊 macOS Sierra 或者 High Sierra，請睇下[依段影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解點樣執行開發者簽署嘅程式。

- macOS 軟件包：[krita-5.1.1.dmg](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.dmg)

### Android

我哋提供嘅 ChomeOS 同 Android 版本仲係**測試版本**。依個版本或可能含有大量嘅 bug，而且仲有部份功能未能正常運作。由於使用者介面仲未改進好，軟件或者須要配合實體鍵盤先可以用到全部功能。Krita 唔啱俾 Android 智能電話用，只係啱平板電腦用，因為使用者介面嘅設計並未為細小嘅螢幕做最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-x86_64-5.1.1-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-x86-5.1.1-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-arm64-v8a-5.1.1-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.1.1/krita-armeabi-v7a-5.1.1-release-signed.apk)

### 原始碼

- [krita-5.1.1.tar.gz](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.tar.gz)
- [krita-5.1.1.tar.xz](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.tar.xz)

### 檔案校對碼 (md5sum)

請瀏覧 [https://download.kde.org/stable/krita/5.1.1/](https://download.kde.org/stable/krita/5.1.1) 並撳落每個下載檔案嘅「Details」連結以查閱嗰個檔案嘅 MD5 / SHA1 / SHA256 校對碼。

### 數碼簽署

Linux AppImage 以及原始碼嘅 .tar.gz 同 .tar.xz 壓縮檔已使用數碼簽署簽名。你可以由[依度](https://files.kde.org/krita/4DA79EDA231C852B)取得 public key。簽名檔可以喺[依度](https://download.kde.org/stable/krita/5.1.1/)揾到（副檔名為 .sig）。
